export default class Registration {
    constructor(name, surname, departament_id, user_id) {
      this.name = name;
      this.surname = surname;
      this.departament_id = departament_id;
      this.user_id = user_id;
    }
  }