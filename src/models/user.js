export default class User {
    constructor(username, password, name, surname, email, departament_id) {
      this.username = username;
      this.email = email;
      this.password = password;
      this.name = name;
      this.surname = surname;
      this.departament_id = departament_id;
    }
  }