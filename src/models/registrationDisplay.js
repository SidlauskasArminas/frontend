export default class Registration {
    constructor(id, name, surname, departament, user, date, time, status, departament_id, dateTime) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.departament = departament;
        this.user = user;
        this.date = date;
        this.time = time;
        this.status = status;
        this.departament_id =  departament_id;
        this.dateTime = dateTime;
    }
  }