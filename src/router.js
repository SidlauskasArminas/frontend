import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Login from './views/Login.vue';
import Register from './views/Register.vue';

Vue.use(Router);

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/home',
      component: Home
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/registry',
      name: 'Registry',
      //lazy-loaded
      component: () => import('./views/Registry.vue')
    },
    {
      path: '/myRegistration',
      name: 'myRegistration',
      props: true,
      //lazy-loaded
      component: () => import('./views/myRegistration.vue')
    },
    {
      path: '/registrations',
      name: 'registrations',
      //lazy-loaded
      component: () => import('./views/Registrations.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      // lazy-loaded
      component: () => import('./views/Profile.vue')
    },
    {
      path: "*",
      name: "/*",
      component: () =>  import('./views/NotFound.vue')
    }
  ]
});

router.beforeEach((to, from, next) => {
    const publicPages = ['/', '/login', '/register', '/home', '/*', '/myRegistration', '/registry'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');

    // trying to access a restricted page + not logged in
    // redirect to login page
    if (authRequired && !loggedIn) {
      next("*");
    } else {
      next();
    }
  });