import axios from 'axios';

// const API_URL = 'http://localhost:5000/api/';

class AuthService {
  login(user) {
    return axios
      .post('auth', {
        username: user.username,
        password: user.password
      })
      .then(response => {
        if (response.data.accessToken) {
          localStorage.setItem('user', JSON.stringify(response.data));
        }

        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('user');
  }

  register(user) {
    return axios.post('users', {
      username: user.username,
      email: user.email,
      password: user.password,
      name: user.name,
      surname: user.surname,
      departament_id: user.departament_id
    });
  }
}

export default new AuthService();